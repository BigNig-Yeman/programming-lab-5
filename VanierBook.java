public class VanierBook {
    
    private String book;
    private String author;
    private String ISBN;
    private int yearOfPublishing;
    private static String publisher = "Vanier";
    private int amountOfBooks = 0;
    
    public String getBook(){
        return book;
    }
    
    public String getAuthor(){
        return author;
    }
    
    public String getISBN(){
        return ISBN;
    }
    
    public int getYearOfPublishing(){
        return yearOfPublishing;
        
    }
    
    
    void setYearOfPublishing(int yearOfPublishing){
        
        if (yearOfPublishing<1970){
            System.out.println("Just a sec..Vanier College was founded in 1970!"
                    + " How could they have published a book before that?");
        }
    }
    
}
